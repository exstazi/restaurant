
var basket = (function($) {

    var basketData;

    function init() {
        _onClickChangeCountInCart();
        _onClickRemoveFromCart();
        getData();

        $('#btn-delivery').click( function() {
            if (getTotalSumm() < 300) {
                $('#min-modal').modal();
            } else {
                document.location.href='delivery.php';          
            }
        });
    }

    function getData() {
        $.post( "ajax/get-basket.php", function( data ) {
            basketData = JSON.parse(data) || [];
            console.log( basketData );
            renderBasket();
        });
    }

    function remove(id) {
        basketData = _.reject(basketData, function(item) {
            return item.id === id;
        });
        $.ajax({
            type: "POST",
            url: "ajax/basket/delete-product.php",
            data: {
                id: id
            }
        });
        return basketData;
    }

    function renderBasket() {
        var template = _.template($('#cart-template').html()),
            data = {
                goods: basketData 
            };
        $('#cart').html( template(data) );
        renderTotalCartSumma();
    }

    function renderTotalCartSumma() {
        $('#total-summa').html( getTotalSumm() );
    }

    function getTotalSumm() {
        return _.reduce(basketData, function(sum, item) {return sum + item.count * item.price}, 0);
    }

    function findCartElemById(id) {
        return $('.js-cart-item' + '[' + 'data-id' + '="'+id+'"]');
    }

    function getById(id) {
        return _.findWhere(basketData, {id: id});
    }

    function changeCount(id, delta) {
        var item = getById(id);
        if(item) {
            item.count = item.count + delta;
            if (item.count < 1) {
                item.count = 1;
            }
            $.ajax({
                type: "POST",
                url: "ajax/basket/change-count.php",
                data: {
                    id: id,
                    count: item.count
                }
            });
        }
        return _.findWhere(basketData, {id: id}) || {};
    }


    function _onClickChangeCountInCart() {
        $('body').on('click', '.js-change-count', function(e) {
            var $this = $(this),
                id = +$this.attr('data-id'),
                delta = +$this.attr('data-delta'),
                $cartElem = findCartElemById(id),
                cartItem = changeCount(id, delta);
            if (cartItem.count) {
                $cartElem.find('.js-count').html(cartItem.count);
                $cartElem.find('.js-summa').html(cartItem.count * cartItem.price);
            }
            renderTotalCartSumma();
        });
    }

    function _onClickRemoveFromCart() {
        $('body').on('click', '.js-remove-from-cart', function(e) {
            var $this = $(this),
                id = +$this.attr('data-id'),
                $cartElem = findCartElemById(id);
            remove(id);
            $cartElem.remove();
            renderTotalCartSumma();
        });
    }

    return {
        init: init
    }
})(jQuery);

jQuery(document).ready(basket.init);