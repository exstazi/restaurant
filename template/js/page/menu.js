
var magazine = ( function($) {

	var products,
		basket;

	var jDivLoading = $('#div-loading');

	//
	var jLinkBasket = $("#link-basket");
	//

	function init() {
		onClickCat();
		onClickOpenDescription();
		onClickToBasket();
		onClickClearBasket();		

		loadMenu(1);
		loadBasket();
	}

	//Загрузка товаров по категории idCat
	function loadMenu(idCat) {
		$.ajax({
			type: "POST",
			url: "/ajax/get-json-products.php",
			data: {
				id: idCat
			},
			success: function(data) {
				products = JSON.parse( data ) || [];				
				jDivLoading.hide();
				renderMenu();
			}
		});
	}

	//Загрузка товаров в корзине
	function loadBasket() {
		$.post( "/ajax/get-json-basket.php", function(data) {
			basket = JSON.parse( data ) || [];
			renderMiniBasket();
			if (basket.length > 0 ) {
				jLinkBasket.css( {'display': 'block'});
			}
		});
	}

	function renderItemBasket() {
		_.each( basket, function( elem ) {
			var jbtn = $('.js-to-basket[data-id=' + elem.id + ']' );
			jbtn.html( "Добавлено" );
			jbtn.css( 'background-color', '#aaa' );
		});
	}


	//Отрисовать текущее меню
	function renderMenu() {
		var template = _.template( $('#item-menu-template').html() );        
        $('#list-product-menu').html( template( {goods: products} ) );
        renderItemBasket();
	}

	//Измненить количество товара в корзине
	function changeBasket(id) {
		var el = _.findWhere(basket, {id: id});
		if (!el) {
			el = _.findWhere( products, {id: id} );
			var pr = {id: id, name: el.name, img: el.img, count: 1, gram: el.gram, price: el.price};
			if (basket) {
				basket.push( pr );
			} else {
				basket = [pr];
			}
		} else {
			el.count = el.count + 1;			
		}
		renderMiniBasket();
	}	

	function animateProductToBasket(id) {
   		var jimg = $('.js-main[data-id=' + id + ']' );
   		var jpos = $('.js-pos[data-id=' + id + ']' );

   		if (!_.findWhere( basket, {id: id})) {
   			jpos = $('#clear-basket');
   		}

		jimg.clone()
            .css({ 'position' : 'absolute',
            	   'z-index' : '999',
            	   'width': 180,
            	   'opacity': 0.2,
            	   'top': jimg.offset().top + 40,
            	   'left': jimg.offset().left + 60
            	})
            .appendTo("body")
            .animate(
            		{
		            	opacity: 0.05,
		                left: jpos.offset()['left'],
		                top: jpos.offset()['top'],
		                width: 20
	            	},
	                800,
	                function() {
	                	$(this).remove();
            		} );
    }

    //Можно будет удалить
    function animateButtonBasket() {
    	jLinkBasket.html( '<center>+1<center>' );

    	jLinkBasket.animate( {fontSize: '27px', specialEasing: 'linear'}, 300, function() {
    		jLinkBasket.css( { 'font-size': '14px'} );
    		jLinkBasket.html( 'Корзина' );
    	} );

    }

	//Показать модальное окно с подробным описание продукта
	function onClickOpenDescription() {
		$('body').on('click', '.js-open-modal', function(e) {
			var product = _.findWhere(products, {id: parseInt( $(this).attr('data-id') )});
			if (!product) {
				product = _.findWhere( basket, {id: parseInt( $(this).attr('data-id')) });
			} 

        	$("#window-title").html( product.name );
			$("#window-gram").html( product.gram + " " + product.units + "." );
			$("#window-price").html( product.price+ " <i class='fa fa-ruble'></i>" );
			$("#window-products").html( product.ingredients );
			$("#window-img").attr( 'src', "/img/" + product.img );
 			$("#myModal").modal('show');
		});
	}

	//Добавить товар в корзину
	function onClickToBasket() {
		$('body').on('click', '.js-to-basket', function(e) {
			var $this = $(this),
       			id = +$this.attr('data-id');
       		
       		animateProductToBasket( id );
       		animateButtonBasket();
       		changeBasket( id );

       		$.ajax({
				type: "POST",
				url: "/ajax/add-product-to-basket.php",
				data: {
					id: id
				},
				success: function( data ) {
					$this.html( "Добавлено" );
       				$this.css( 'background-color', '#aaa' );
				}
			});
		});
	}

	function onClickCat() {
		$('body').on('click', '.cat-a', function() {
			var id = $(this).attr('data-id');
			$('#list-product-menu').html( '' );
			jDivLoading.show();
			loadMenu( id );			
		});
	}

	function onClickClearBasket() {
		$('#clear-basket').click( function() {
			$.post( "/ajax/delete-basket.php", function( data ) {
				$('#mini-basket').html( '' );
				delete basket;
				basket = [];
				$('.js-to-basket').each( function() {
					$(this).removeAttr( 'style' );
					$(this).html( 'В корзину' );
				});
			} );
		});
	}

	function renderMiniBasket() {
		if (basket.length > 0) {
			var template = _.template($('#mini-basket-template').html());
		    $('#mini-basket').html( template({goods: basket}) );		    		
		}
	}

	return {
		init: init
	}

})(jQuery);

jQuery(document).ready(magazine.init);
/*


var i = 14;
	var ids = 0, id2;

$('body').on('click', '.js-to-basket', function(e) {
	var $this = $(this),
        id = +$this.attr('data-id');

    $.ajax({
		type: "POST",
		url: "/ajax/add-to-backet.php",
		data: {
			id: id
		},
		success: function( data ) {
			//
			i = 14;


			clearInterval(ids);
			clearTimeout(id2);
			
			document.getElementById( 'link-basket').style.display = 'block';
			//document.getElementById( "backet-products" ).innerHTML = data;
			document.getElementById( "link-basket" ).innerHTML = "<center>+1<center>";

			if (ids== 0 ) {
				ids = setInterval( function() {
					document.getElementById( "link-basket" ).style.fontSize = i+'px';
					++i;
				}, 1000 / 60 );
			}
			id2 = setTimeout( function() {
					document.getElementById( "link-basket" ).innerHTML = "Корзина";
					document.getElementById( "link-basket" ).style.fontSize ='14px';
					clearInterval( ids );
					ids = 0;
				}, 300);				
		}
	});
});*/
