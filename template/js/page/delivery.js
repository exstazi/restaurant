$('#checkout').on('click', function () {
    var jname = $("#name"),
        jaddress = $("#address"),
        jphone = $('#phone');

    if (jname.val().trim().length < 3) {
    	jname.css({'border-color':'#d8512d'});
    	return ;
    } else {
    	jname.removeAttr( 'style' );
    }

    if (jphone.hasClass('empty') || jphone.val().length == 0) {
        jphone.css({'border-color':'#d8512d'});
        return ;
    } else {
        jphone.removeAttr( 'style' );
    }

    if (jaddress.val().trim().length < 3) {
        jaddress.css({'border-color':'#d8512d'});
        return ;
    } else {
        jaddress.removeAttr( 'style' );
    }

    $.ajax({
        type: "POST",
        url: "/ajax/checkout-delivery.php",
        data: {
            name: jname.val(),
            address: jaddress.val(),
            phone: jphone.val(),
            payment: $('#payment').val(),
            bills: $('#bills').val(),
            count_person: $('#count_person').val(),
            comment: $('#comment').val()
        },
        success: function( data ) {
            var answer = JSON.parse( data ) || [];
            if (answer.error == false) {
                location.href = 'finish';
            }
   	    }
    });

});

$('#phone').mask('+7 (000) 000-00-00', {
    onComplete: function() {
        $('#phone').removeClass( 'empty' );
    },
    onChange: function() {
        $('#phone').addClass( 'empty' );
    }
});