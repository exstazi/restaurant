
var magazine = ( function($) {

	var basket;

	function init() {
		updateBasket();
		onClickChangeCount();
		onClickClearBasket();
		onClickRemoveFromCart();
	}

	//Отрисовать корзину
	function renderBasket() {
		var template = _.template( $('#basket-template').html() );        
        $('#basket').html( template( {goods: basket} ) );

       
	}

	function renderTotalCartSumma() {
        $('#total-summa').html( getTotalSumm() );
         if ($(document).height() <= $(window).height()) {
            $(".site-footer").addClass("navbar-fixed-bottom");
        } else {
            $('.site-footer').removeClass('navbar-fixed-bottom');
        }
    }

    function getTotalSumm() {
        return _.reduce(basket, function(sum, item) {return sum + item.count * item.price}, 0);
    }

	function changeCount(id, delta) {
        var item = _.findWhere(basket, {id: id});
        if(item) {
            item.count = item.count + delta;
            if (item.count < 1) {
                item.count = 1;
            }
            $.ajax({
                type: "POST",
                url: "/ajax/edit-count-basket.php",
                data: {
                    id: id,
                    count: item.count
                }
            });
        }
        return item || {};
    }

    function remove(id) {
        basket = _.reject(basket, function(item) {
            return item.id === id;
        });
        $.ajax({
            type: "POST",
            url: "/ajax/delete-basket.php",
            data: {
                id: id
            }
        });
    }

	function updateBasket() {
		$.post( "/ajax/get-json-basket.php", function(data) {
			basket = JSON.parse( data ) || [];			
			renderBasket();
			renderTotalCartSumma();
		});
	}

	function findCartElemById(id) {
        return $('.js-cart-item[data-id="'+id+'"]');
    }

	function onClickChangeCount() {
		$('body').on('click', '.js-change-count', function(e) {
            var $this = $(this),
                id = +$this.attr('data-id'),
                delta = +$this.attr('data-delta'),
                $cartElem = findCartElemById(id),
                cartItem = changeCount(id, delta);
            if (cartItem.count) {
                $cartElem.find('.js-count').html(cartItem.count);
                $cartElem.find('.js-summa').html(cartItem.count * cartItem.price);
            }
            renderTotalCartSumma();
        });
	}

	function onClickRemoveFromCart() {
        $('body').on('click', '.js-remove-from-cart', function(e) {
            var $this = $(this),
                id = +$this.attr('data-id'),
                $cartElem = findCartElemById(id);
            remove(id);
            $cartElem.remove();
            renderTotalCartSumma();
        });
    }

	function onClickClearBasket() {
		$('#clear-basket').click( function() {
			$.post( "/ajax/delete-basket.php", function( data ) {
				delete basket;
				basket = [];
				renderBasket();
				renderTotalCartSumma();
                location.href = '/menu';
			} );
		});
	}

	return {
		init: init
	}

})(jQuery);

jQuery(document).ready(magazine.init);