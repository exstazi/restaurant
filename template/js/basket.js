(function( $ ){
	$(function() {
		$('.js-validate-form').each(function(){
			var form = $(this),
				btn = form.find('.btn-send');

			form.find('.js-check-text').addClass('empty_field');
			form.find('.js-check-phone').addClass('empty_field');

			function checkInput() {
				form.find('.js-check-text').each(function() {
					if($(this).val().trim().length > 2) {
						$(this).removeClass('empty_field');
					} else {
						$(this).addClass('empty_field');
					}
				});
				form.find('.js-check-number').each(function() {
					var value = $(this).val();
					if (isNaN(parseInt(value))) {
						value = 1;
					} else if ( parseInt(value) != value ) {
						value = parseInt(value);
					}
					value = (value < 1 ? 1 : (value > 25) ? 25: value);

					if ($(this).val() != value) {
						$(this).val(value);
					}
				});
			}

			function lightEmpty() {
				form.find('.empty_field').css({'border-color':'#d8512d'});				
				setTimeout(function(){
					form.find('.empty_field').removeAttr('style');
				},1000);
			}

			setInterval(function(){
				checkInput();
				var sizeEmpty = form.find('.empty_field').size();
				if(sizeEmpty > 0){
					if(btn.hasClass('disabled')){
						return false
					} else {
						btn.addClass('disabled')
					}
				} else {
					btn.removeClass('disabled')
				}
			},1000);

			btn.click(function(){
				if($(this).hasClass('disabled')){
					lightEmpty();
					return false
				} else {
					form.submit();
				}
			});
		});
	});
})( jQuery );

/*
$('#btn-send').click( function() {
	$('.js-check-text').each( function(){
		if ($(this).val().trim().length > 2) {
			$(this).removeClass('empty_field');
			$(this).removeAttr('style');
		} else {
			$(this).addClass('empty_field');
			$(this).css({'border-color':'#d8512d'});
		}
	});	

	$('.js-check-number').each( function(){
		
	});

	if ( $('.rf').find('.empty_field').size() == 0) {
		$('.rf').submit();
	}

});
*/
jQuery(function($) {
	$(".js-check-phone").mask( "+7 (999) 999-99-99",{autoclear:false,completed:function(){$(this).removeClass('empty_field');}});
});