<div class="container">

	<?php
		if ( !empty( $CAT) ) {
			$share = $db->getRow( "SELECT * FROM shares WHERE active=0 AND id=?i", $CAT );
			if ( empty( $share) ) {
				echo "<h1>Данной акции не существует</h1>
					  <a href='/shares'> ← Смотреть другие акции</a>";
			} else {
				echo '<div class="row"><div class="col-md-10 col-md-offset-1 about-col"><img src="/img/'.$share['img'].'.jpg" /><br /><br />
					<small>'.$share['date'].'</small>
					<p class="text-justify">'.$share['description'].'</p>
					<a href="/shares"> ← Смотреть другие акции</a></div></div>';
			}
			
		} else {
			function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
				$text = strip_tags($text);
				$text = substr($text, 0, $counttext);
				$text = rtrim($text, "!,.-");
				$text = substr($text, 0, strrpos($text, ' '));
				return $text."… ";
			}

			$shares = $db->getAll("SELECT * FROM shares WHERE active=0");
			for ($i = 0; $i < sizeof( $shares ); ++$i) {
				echo "<div class='row'>		
						<div class='col-md-6 about-col'>
							<img src='/img/".$shares[$i]['img'].".jpg' />						
						</div>
						<div class='col-md-6 about-col'>
							<h2> ".$shares[$i]['name']." </h2>
							<h4>".$shares[$i]['date']."</h4>
							<p>".maxsite_str_word( $shares[$i]['description'], 1500 )."</p>
							<a class='hl-button' href='/shares/".$shares[$i]['id']."'>Подробнее</a>
						</div>
					</div>";
			}
		}
	?>
	<br /><br /><br /><br /><br /><br />
</div>