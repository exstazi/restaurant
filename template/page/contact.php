<br /><br />
<div class="row">
	<div class="col-md-8">
		<div class="location-box">
			<div id="map" style="width: 100%"></div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="address-box">
			<h3> Адрес </h3>
			<?=$global['city'];?> <br>
			<?=$global['address'];?><br>
		</div>
		<div class="phone-details">
			Режим работы:<br /><b>Будни: <?=$global['mode'];?><br />Выходные: <?=$global['mode_weekend'];?></b><hr />
			Телефон - <?=$global['phone'];?> <br>
			Телефон контроля качества - <?=$global['control-phone'];?> <br>
		</div>
	</div>
</div>
