<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
<br /><br />
	<table>
		<tr>
			<td><input type="text" class="form-control"  id="name" placeholder="Введите имя" /></td>
			<td>Способ оплаты: </td>
			<td><select id="payment" name="payment" class="form-control ">
					<option value="1">Наличными</option>
					<option value="0">По карте</option>
				</select> </td>
		</tr>
		<tr>
			<td><input type="text" class="form-control" id="phone" name="phone" placeholder="Введите телефон"/></td>
			<td>Сдача с купюры</td>
			<td>
				<div class="input-group">
				  <input id="bills" type="number" min="0" class="form-control " name="bills" />
				  <span class="input-group-addon"> <i class='fa fa-ruble' ></i></span>
				</div>
			</td>
		</tr>
		<tr>
			<td><input type="text" class="form-control" id="address" name="address" placeholder="Введите адрес"  /></td>
			<td>Количество персон </td>
			<td>
			<input id="count_person" class="form-control" type='number' value='1' name="count_person" /></td>
		</tr>
		<tr>
			<td colspan="3"><textarea id="comment" class="form-control" rows="5"  placeholder="Добавить комментарий к заказу..."></textarea></td>
		</tr>

		<tr style="background-color: #eee">
			<td><h3>Общая сумма заказа</h3><h3>Доставка</h3></td>
			<td></td>
			<td><h3><?=$totalPrice;?> р.</h3> <h3>0 р.</h3></td>
		</tr>

		<tr style="background-color: #ccc">
			<td><h3>Итого</h3></td>
			<td></td>
			<td><h3><?=$totalPrice;?> р.</h3></td>
		</tr>

		<tr>
			<td></td>
			<td></td>
			<td><button id="checkout" class='btn-basket'>Оформить заказ</button></td>
		</tr>
		
	</table>

</div>
<div class="col-md-2"></div>
</div>			
