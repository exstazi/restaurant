<br /><br />
<table class="table-basket">
	<thead>
		<tr>
		<th>Блюдо</th>
		<th>Наименование</th>
		<th>Количество</th>
		<th>Стоимость</th>
		<th></th>
	</tr>			
	</thead>
	
	<tbody id="basket">
        <tr><td colspan="6"><img src="/template/img/loading.gif" /></td></tr>
    </tbody>

	<tr>
		<td></td>
		<td></td>
		<td><h3>Общая сумма заказа</h3></td><td><h3><span id='total-summa'>0</span> р.</h3></td>
		<td></td>
	</tr>
</table>


<div class="container">
	<div class="row">
		<div class="col-md-2">
			<button id="clear-basket" href="">Очистить корзину</button>
		</div>

		<div class="col-md-3 col-md-offset-3">
			<a class='btn-basket' href="/menu">Вернуться в меню</a>
		</div>
		<div class="col-md-2">
				<a class="btn-basket" href="/pickup">Самовывоз</a><br /><p class="text-center">Заберите когда удобно</p>
			</div>
			<div class="col-md-2">
				<a id="btn-delivery" class="btn-basket" href="/delivery">Доставка</a><br /><p class="text-center">Привезем домой в удобное время</p>
			</div>
	</div>
</div>
<br />
<script id="basket-template" type="text/template">
    <% _.each(goods, function(good) { %>
    	<tr class="cart-item js-cart-item" data-id="<%= good.id %>">
    		<td width=180><img src='img/<%= good.img %>' /></td>
			<td><h3> <%= good.name %></h3>Вес: <%= good.gram %> <%= good.units %>.</td>							
			<td valign='middle' align='center'>
				<span 
                    class="cart-item__btn-dec-count js-change-count" 
                    title="Уменьшить на 1" 
                    data-id="<%= good.id %>" 
                    data-delta="-1"
                >
                
                    <i class="fa fa-minus btn"></i>
                </span>
                <span class="js-count"><%= good.count %></span>
                <span 
                    class="cart-item__btn-inc-count js-change-count" 
                    title="Увеличить на 1" 
                    data-id="<%= good.id %>" 
                    data-delta="1"
                >
                    <i class="fa fa-plus btn"></i>
                </span>
			</td>
			<td valign='middle'>
				<h4><span class="js-summa"><%= good.count * good.price %></span> р.</h4>
			</td>
			<td>
                <span class="cart-item__btn-remove js-remove-from-cart" title="Удалить из корзины" data-id="<%= good.id %>">
                    <i class="fa fa-times btn"></i>                             
                </span>
            </td>
        </tr>
   	 <% }); %>
</script>