<?php
	session_start();
	include '/template/module.local.storage.php';
	include '/template/module.page.php';

	$global_param = Array
		(
			"path-to-global" => "./template/config/global.json",
			"path-to-categoria" => "./template/config/categoria.json",
			"path-to-pages" => "./template/config/pages.json"
		);

	class Magazine
	{
		private $curr_page;
		private $url;
		private $storage_params;

		private $all_params;

		function __construct() {
			global $global_param;

			$this->storage_params = new LocalStorage( $global_param['path-to-global'] );

			$this->curr_page = new Page( $global_param['path-to-pages'] );
			$this->proccessingURL();
			$this->curr_page->setCurrentPage( $this->url['page'], $this->isWorkTime(), $this->isEmptyBasket() );
		}

		public function getParamPage() {
			if (empty($this->all_params)) {
				$this->all_params = array_merge( $this->curr_page->getInfo(), $this->storage_params->getAll() );
				unset( $this->all_params['access'] );
			}
			return $this->all_params;
		}

		private function proccessingURL() {
			$this->url = Array
					( 
						'page' => 'menu'
					);

			if ($_SERVER['REQUEST_URI'] != '/') {
				$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
				$uri_parts = explode('/', trim($url_path, ' /'));

				$this->url['page'] = $uri_parts[0];
				$this->url['param-page'] = array_slice( $uri_parts, 1);
			}
		}

		private function isWorkTime() {
			$time = date( "H" );
			return ( $time > $this->storage_params->getById('work_start') 
						&& $time < $this->storage_params->getById('work_stop') );
		}

		private function isEmptyBasket() {
			return !( !empty( $_SESSION['basket']) && sizeof( $_SESSION['basket']) > 0 );
		}
	}
	

	class Main
	{
		private $pages;
		private $config;

		private $global_config;

		function __construct() {
			$this->loadInfoPages();
			$this->loadGlobalConfig();

			$this->setParamForCurrentPage();
		}

		public function getInfoPage( $param ) {
			if (!empty($this->global_config[$param])) {
				return $this->global_config[$param];
			}
			return $this->config[$param];
		}

		public function getPagesInMenu() {
			$menu_pages = Array();
			foreach ($this->pages as $key => $value) {
				if (!$value['access']['view-in-menu']) {
					continue ;
				}
				$menu_pages[] = Array
					( 
						'title' => $value['title'],
						'url' => $key
					);
			}
			return $menu_pages;
		}

		public function isWorkTime() {
			$time = date( "H" );
			return ( $time > 10 && $time < 22 );
		}

		public function isEmptyBasket() {
			return !( !empty( $_SESSION['basket']) && sizeof( $_SESSION['basket']) > 0 );
		}

		private function setParamForCurrentPage() {
			$url = $this->processingURL();

			if (empty( $url['page']) || empty( $this->pages[$url['page']])) {
				$url['page'] = 'menu';
			}

			$this->config = $this->pages[$url['page']];

			if (!$this->config['access']['visable'] || ($this->isEmptyBasket() && !$this->config['access']['empty-basket']) ) {
				$this->config = $this->pages[$this->global_config['rederict_main_page']];
			} else if ( !$this->isWorkTime() && !$this->config['access']['not-work-time'] ) {
				$this->config = $this->pages[$this->global_config['rederict_not_work_time_page']];
			}
		}

		private function loadInfoPages() {
			$this->pages = json_decode( file_get_contents( "./template/config/pages.json" ), true );
		}

		private function loadGlobalConfig() {
			$this->global_config = json_decode( file_get_contents( "./template/config/global.json " ), true );
		}

		private function processingURL() {
			$url = Array();

			if ($_SERVER['REQUEST_URI'] != '/') {
				$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
				$uri_parts = explode('/', trim($url_path, ' /'));

				$url['page'] = $uri_parts[0];
				$url['param-page'] = array_slice( $uri_parts, 1);
			}
			return $url;
		}
	}

?>