<?php
class Page 
{
	private $storage_pages;
	private $current_page;

	private $error_page_404 = Array
					(
						'title' => 'Ошибка. Страница не найдена',
						'keyword' => '...',
						'description' => '...',
						'php-files' => Array( 'error_404' ),
						'js-files' => Array( 'error' )
					);
	private $error_page_not_work = Array
					(
						'title' => 'Ошибка. Магазин работает с 11 до 22',
						'keyword' => '...',
						'description' => '...',
						'php-files' => Array( 'error_not_work' ),
						'js-files' => Array( 'error' )
					);

	function __construct($path) {
		$this->storage_pages = new LocalStorage( $path );
	}

	public function setCurrentPage($url, $is_work_time, $is_empty_basket ) {
		$this->current_page = $this->storage_pages->getById( $url );
		if (sizeof( $this->current_page) < 1 
				|| !(isset( $this->current_page['access']) && $this->current_page['access']['visable'] ) ) {
			$this->current_page = $this->error_page_404;
		} else if (!$is_work_time && !$this->current_page['access']['not-work-time']) {
			$this->current_page = $this->error_page_not_work;
		} else if ($is_empty_basket && !$this->current_page['access']['empty-basket']) {
			$this->current_page = $this->storage_pages->getById('menu');
		}
	}

	public function getInfo($param = '') {
		if (strlen( $param ) > 1) {
			return $this->current_page[$param];
		}
		return $this->current_page;
	}

}
?>