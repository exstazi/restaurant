<?php
session_start();

class Basket
{
	private $db;
	private $full_price;

	function __construct($db) {
		$this->db = $db;
		$this->full_price = 0;
	}

	public function insertCurrentOrder( $name, $phone, $payment = 1, $address = "", $bills = 0, $count_person = 0, $comment = "" )
	{
		if (!$this->isEmptyBasket()) {
			$day = date("Y-m-d");
			$time = date("H:i:s");
			$list = $this->serializeListProduct();
			$number = $this->getNumberOrder($day);

			$sql = "INSERT INTO orders SET nid=?i, name=?s, phone=?s, list_products=?s, full_price=?i, day=?s, time=?s, payment=?i, address=?s, bills=?i, count_person=?i, comment=?s";

			$payment = (int)$payment;
			$bills = (int)$bills;
			$count_person = (int)$count_person;

			$this->db->query( $sql, $number, $name, $phone, $list, $this->full_price, $day, $time,
								$payment, $address, $bills, $count_person, $comment );
			$this->clearSession( $number );
		}		
	}

	public function isEmptyBasket() {
		if ( sizeof( $_SESSION['basket']) < 1 ) {
			return true;
		}
		return false;
	}	

	private function clearSession($number) {
		unset( $_SESSION['order'] );
		$_SESSION['order'] = $_SESSION['basket'];
		$_SESSION['order-number'] = $number;
		unset( $_SESSION['basket'] );
	}

	private function getNumberOrder($day) {
		$count = $this->db->getRow( "SELECT COUNT(*) FROM orders WHERE day=?s", $day );
		return $count['COUNT(*)'] + 1;
	}

	private function serializeListProduct() {
		$list = "";
		foreach ($_SESSION['basket'] as $key => $value) {
			$list .= $value['id'] . '-' . $value['count'] . '|';
			$this->full_price += $value['price'] * $value['count'];
		}
		$list = substr( $list, 0, -1 );
		return $list;
	}
}

?>