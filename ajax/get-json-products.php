<?php
	include_once "../template/db.php";
	include_once "../template/config/categoria.php";

	$db = new SafeMySQL();
	$products = $db->getAll("SELECT * FROM product WHERE cat=?i",$_POST['id']);
	$q = false;
	echo "[";
	for ($i = 0; $i < sizeof( $products ); ++$i) {
		$value = $products[$i];
		if ($q) { echo ","; } $q = true; 
		echo '{"id":'.$value['id'].',
			   "cat": '.$value['cat'].',
			   "name":'.json_encode( $value['name'] ).',
			   "img":"'.$value['img'].'",
			   "price":'.$value['price'].',
			   "gram":'.$value['gram'].',
			   "ingredients":'.json_encode( $value['ingredients'] ).',
			   "units": "'.$cat[$value['cat']]['units'].'"}';
	}
	echo "]";
?>