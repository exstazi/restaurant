<?php
	session_start();
	if (!empty( $_POST['id'] )) {
		$id = $_POST['id'];		

		if (empty($_SESSION['basket'][$id])) {
			include_once "../template/db.php";
			include_once "../template/config/categoria.php";
			$db = new SafeMySQL();
			$result = $db->getRow( "SELECT id, name, img, price, gram, cat, ingredients FROM product WHERE id = ?i", $id );

			$_SESSION['basket'][$id] = Array 
				(
					'id' => $id,
					'name' => $result['name'],
					'img' => $result['img'],
					'count' => 1,
					'price' => $result['price'],
					'gram' => $result['gram'],
					'ingredients' => json_encode( $result['ingredients'] ),
					'units' => $cat[$result['cat']]['units']
				);	
		} else {
			++$_SESSION['basket'][$id]['count'];
		}
	} 
?>