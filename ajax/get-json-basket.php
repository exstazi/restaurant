<?php
	session_start();
	if (sizeof( $_SESSION['basket']) > 0) {
		$q = false;
		echo "[";
		foreach ($_SESSION['basket'] as $key => $value) {
			if ($q) { echo ","; } $q = true; 
			echo '{ "id":'.$key.', 
					"name":"'.$value['name'].'", 
					"img":"'.$value['img'].'", 
					"count":'.$value['count'].', 
					"price":'.$value['price'].', 
					"ingredients":'.$value['ingredients'].',
					"gram":'.$value['gram'].', 
					"units": "'.$value['units'].'"}';
		}
		echo "]";
	} else {
		echo "[]";
	}
?>
