<?php
	session_start();
	if (empty( $_POST['name']) || strlen( $_POST['name'] ) < 3 
				|| empty( $_POST['phone']) || strlen( $_POST['phone']) != 18 ) {
		echo '{"error":true}';
	} else {
		include_once "../template/db.php";
		include_once "../template/module.basket.php";
		$basket = new Basket( new SafeMySQL() );
		$basket->insertCurrentOrder( $_POST['name'], $_POST['phone'], 0 );
		echo '{"error":false}';
	}
?>