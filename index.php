<?php
	session_start();
	include_once "/template/module.global.php";
	$mag = new Magazine();
	$page = $mag->getParamPage();

?>

<!DOCTYPE html>
<html lang="ru-RU">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?=$page['title'];?> - <?=$page['name'];?></title>

	<link rel="stylesheet" type="text/css" href="/template/css/main.css" media="all" />
	<link rel="stylesheet" type="text/css" href="/template/css/template.css" media="all" />	
	<link rel='stylesheet' id='custom-css'  href='/template/css/custom62d0.css?ver=4.5.3' type='text/css' media='all' />
	<link rel="stylesheet" href="/template/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/template/css/style.css" media="all" />	
</head>
<body>

	<div class="top-bar" style="position: fixed; width: 100%; z-index: 100">		
		<div class="container"><div class="row">

			<div class="col-sm-12 top-right">
				<ul class="ul-header">
					<li><a  id="link-basket"  class="basket-button"  href="/basket"> Корзина </a></li>
					<li><i class="fa fa-phone"> </i> Контроль качества: <?=$page['control-phone'];?></li>	
					<li><i class="fa fa-phone"> </i> <?=$page['phone'];?></li> 
					<li> <i class="fa fa-clock-o"> </i> Режим работы: <?=$page['mode'];?></li>
					<li><?=$page['city'];?></li>								
				</ul>				
			</div>
		</div></div>
	</div>
	<br /><br />

	<header class="site-header">
		<div class="container"> <div class="row">
			<div class="col-sm-4"> 
				<div class="site-branding">
					<h1 class="site-title logo">
						<a href="index.php"><img class="ft_logo" src="/template/img/logo.jpg" width="250" /></a>
					</h1>
				</div>
			</div>

			<div class="col-sm-8"> 
				<nav id="site-navigation" class="main-navigation" role="navigation" >
					<div class="menu-restaurant-menu-container">
						<ul id="restaurant" class="menu">
							<li class="menu-item"><a href="/menu">Меню</a></li>
							<li class="menu-item"><a href="/shares">Акции</a></li>
							<li class="menu-item"><a href="/about">О нас</a></li>
							<li class="menu-item"><a href="/shipping">Доставка и оплата</a></li>
							<li class="menu-item"><a href="/contact">Контакты</a></li>
							<?php
								/*$pages = $page->getPagesInMenu();
								foreach ($pages as $key => $value) {
									echo '<li class="menu-item"><a href="/'.$value['url'].'">'.$value['title'].'</a></li>';
								}*/
							?>
						</ul>
					</div>				
				</nav>				
			</div>	
		</div></div>
	</header>

	<div class="top-header">
		<div class="container"> 
			<h1><?=$page['title'];?></h1>
		</div>
	</div>

	<div class="container">
		
		<?php
			foreach ($page['php-files'] as $key) {				
				require_once "/template/page/" . $key . ".php";
			}
		?>

	</div>

	<footer class="site-footer">
		<div class="container"> <div class="row"> 
			<div class="col-md-12">
				<div class="site-info">
				Copyright &copy; 2016 <?=$page['name']?> <br>
				  <?=$page['city'];?>
				</div>
			</div>	
		</div></div>		
	</footer>


	<script src="/template/js/jquery.min.js"></script>
    <script src="/template/js/bootstrap.min.js"></script>
    <?php
    	foreach ($page['js-files'] as $key) {				
			echo '<script src="/template/js/'.$key.'.js"></script>';
		}
    ?>

	<script type="text/javascript">
		if ($(document).height() <= $(window).height()) {
	  		$(".site-footer").addClass("navbar-fixed-bottom");
	  	}
	</script>
</body>
</html>
